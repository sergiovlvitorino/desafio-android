package br.com.challenges.vitorino.gitranking.controller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.util.List;

import br.com.challenges.vitorino.gitranking.bean.PullRequest;
import br.com.challenges.vitorino.gitranking.connect.IResultHandler;
import br.com.challenges.vitorino.gitranking.connect.Requester;
import br.com.challenges.vitorino.gitranking.connect.StringResultHandler;
import br.com.challenges.vitorino.gitranking.parser.PullRequestParser;
import br.com.challenges.vitorino.gitranking.util.Constants;

/**
 * Created by sergio on 20/02/16.
 */
public class PullRequestCtrl {

    private Requester requester;
    private PullRequestParser pullRequestParser;

    public PullRequestCtrl(){
        this.requester = new Requester();
        this.pullRequestParser = new PullRequestParser();
    }


    public List<PullRequest> getPullRequests(String nicknameOwner, String repositoryTitle) throws JSONException, ParseException {
        StringBuilder urlPullRequests = new StringBuilder(Constants.URL_PULL_REQUESTS);
        urlPullRequests.append(nicknameOwner);
        urlPullRequests.append(Constants.SLASH);
        urlPullRequests.append(repositoryTitle);
        urlPullRequests.append(Constants.SLASH);
        urlPullRequests.append(Constants.PULLS);
        IResultHandler<String> resultHandler = new StringResultHandler();
        requester.doGet(urlPullRequests.toString(), resultHandler);
        String requestResult = resultHandler.getResult();
        JSONArray requestResultJSONArray = new JSONArray(requestResult);
        return pullRequestParser.convertJSONArrayToPullRequestList(requestResultJSONArray);
    }

}
