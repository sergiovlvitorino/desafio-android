package br.com.challenges.vitorino.gitranking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.challenges.vitorino.gitranking.bean.Repository;
import br.com.challenges.vitorino.gitranking.builder.AlertDialogBuilder;
import br.com.challenges.vitorino.gitranking.controller.RepositoryCtrl;
import br.com.challenges.vitorino.gitranking.adapter.RepositoriesAdapter;
import br.com.challenges.vitorino.gitranking.util.Constants;
import br.com.challenges.vitorino.gitranking.builder.ProgressDialogBuilder;
import br.com.challenges.vitorino.gitranking.connect.InternetChecker;

public class MainActivity extends AppCompatActivity {

    private RepositoryCtrl repositoryCtrl;
    public final int INITIAL_PAGE = 1;
    private int currentPage = INITIAL_PAGE;
    private ListView listView;
    private RepositoriesAdapter repositoriesAdapter;
    private final static List<Repository> repositories = new ArrayList<>();
    private ProgressDialog progressDialog;
    private static boolean firstExecution = true;
    private InternetChecker internetChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_three_lines));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new AlertDialogBuilder().build(MainActivity.this).show();
            }
        });

        initializer();

        if (firstExecution){
            firstExecution = false;
            progressDialog.show();
            loadList(INITIAL_PAGE);
        } else {
            reRenderingTable(repositories, 0);
        }

    }

    private void loadList(final int page) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (internetChecker.hasActiveInternetConnection()){
                    try {
                        renderingTable(repositoryCtrl.getRepositories(page));
                    } catch (Exception e) {
                        launchToast(getResources().getString(R.string.error));
                    }
                } else {
                    launchToast(getResources().getString(R.string.no_internet_connection));
                }
                progressDialog.dismiss();
            }
        }).start();
    }

    public void renderingTable(final List<Repository> repositoriesInner){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                repositories.addAll(repositoriesInner);
                repositoriesAdapter.addAll(repositoriesInner);
            }
        });
    }

    public void reRenderingTable(final List<Repository> repositoriesInner, final int currentPosition){
        if (repositoriesInner.isEmpty()){
            loadList(INITIAL_PAGE);
        } else {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    repositoriesAdapter.addAll(repositoriesInner);
                    listView.setVerticalScrollbarPosition(currentPosition);
                }
            });
        }

    }

    private void initializer() {
        internetChecker = new InternetChecker(this);
        progressDialog = new ProgressDialogBuilder().build(this);
        repositoryCtrl = new RepositoryCtrl();
        listView = (ListView) findViewById(R.id.repository_list);
        repositoriesAdapter = new RepositoriesAdapter(MainActivity.this);
        listView.setAdapter(repositoriesAdapter);
        listView.setOnScrollListener(new AbsListView.OnScrollListener() {

            private int visibleThreshold = 5;
            private int previousTotal = 0;
            private boolean loading = true;

            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState) {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount) {

                if (loading) {
                    if (totalItemCount > previousTotal) {
                        loading = false;
                        previousTotal = totalItemCount;
                    }
                }
                if (!loading && (totalItemCount - visibleItemCount) <= (firstVisibleItem + visibleThreshold)) {
                    loadList(++currentPage);
                    loading = true;
                }
            }
        });

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Repository repository = repositoriesAdapter.getItem(position);
                Intent intent = new Intent(MainActivity.this, PullRequestActivity.class);
                intent.putExtra(Constants.OWNER, repository.getNicknameOwner());
                intent.putExtra(Constants.TITLE, repository.getTitle());
                startActivity(intent);
                MainActivity.this.overridePendingTransition(0, 0);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.CURRENT_POSITION, listView.getFirstVisiblePosition());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        reRenderingTable(repositories, savedInstanceState.getInt(Constants.CURRENT_POSITION));
        super.onRestoreInstanceState(savedInstanceState);
    }

    public void launchToast(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(MainActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });

    }
    
}
