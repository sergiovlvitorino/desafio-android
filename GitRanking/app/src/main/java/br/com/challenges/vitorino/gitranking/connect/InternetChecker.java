package br.com.challenges.vitorino.gitranking.connect;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;

import java.net.HttpURLConnection;
import java.net.URL;

import br.com.challenges.vitorino.gitranking.util.Constants;

/**
 * Created by sergio on 21/02/16.
 */
public class InternetChecker {

    private Context context;

    public InternetChecker(Context context){
        this.context = context;
    }

    public boolean hasActiveInternetConnection() {
        if (isNetworkAvailable()) {
            try {
                HttpURLConnection urlc = (HttpURLConnection) (new URL(Constants.GOOGLE).openConnection());
                urlc.setRequestProperty(Constants.USER_AGENT, Constants.TEST);
                urlc.setRequestProperty(Constants.CONNECTION, Constants.CLOSE);
                urlc.setConnectTimeout(1500);
                urlc.connect();
                return (urlc.getResponseCode() == 200);
            } catch (Exception e) {
            }
        } else {
        }
        return false;
    }

    private boolean isNetworkAvailable() {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

}
