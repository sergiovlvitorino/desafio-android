package br.com.challenges.vitorino.gitranking.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import java.util.List;

import br.com.challenges.vitorino.gitranking.bean.Repository;
import br.com.challenges.vitorino.gitranking.builder.RepositoryTableRowBuilder;

/**
 * Created by sergio on 19/02/16.
 */
public class RepositoriesAdapter extends ArrayAdapter<Repository> {

    private RepositoryTableRowBuilder repositoryTableRowBuilder;

    public RepositoriesAdapter(Context context){
        super(context, 0);
        repositoryTableRowBuilder = new RepositoryTableRowBuilder(LayoutInflater.from(context));
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Repository repository = getItem(position);
        if (convertView == null){
            convertView = repositoryTableRowBuilder.build(repository);
        } else {
            repositoryTableRowBuilder.build(convertView, repository);
        }
        return convertView;
    }
}
