package br.com.challenges.vitorino.gitranking.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.net.ContentHandler;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import br.com.challenges.vitorino.gitranking.bean.PullRequest;
import br.com.challenges.vitorino.gitranking.util.Constants;

/**
 * Created by sergio on 20/02/16.
 */
public class PullRequestParser {

    private SimpleDateFormat simpleDateFormat;

    public PullRequestParser(){
        this.simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    }

    public List<PullRequest> convertJSONArrayToPullRequestList(JSONArray jsonArray) throws JSONException, ParseException {
        List<PullRequest> pullRequests = new ArrayList<>();
        for (int i = 0; i < jsonArray.length(); i++){
            pullRequests.add(convertJSONObjectToPullRequest(jsonArray.getJSONObject(i)));
        }
        return pullRequests;
    }

    private PullRequest convertJSONObjectToPullRequest(JSONObject jsonObject) throws JSONException, ParseException {
        String title = jsonObject.getString(Constants.TITLE);
        String body = jsonObject.getString(Constants.BODY);
        String htmlUrl = jsonObject.getString(Constants.HTML_URL);
        jsonObject.getString(Constants.CREATED_AT);
        JSONObject userJSONObject = jsonObject.getJSONObject(Constants.USER);
        String avatarUrl = userJSONObject.getString(Constants.AVATAR_URL);
        String login = userJSONObject.getString(Constants.LOGIN);
        boolean closed = jsonObject.getString(Constants.CLOSED_AT) == null;
        String createdAtString = jsonObject.getString(Constants.CREATED_AT);
        Calendar createdAt = Calendar.getInstance();
        createdAt.setTime(simpleDateFormat.parse(createdAtString));
        return new PullRequest(title, body, htmlUrl, login, avatarUrl, createdAt, closed);
    }

}
