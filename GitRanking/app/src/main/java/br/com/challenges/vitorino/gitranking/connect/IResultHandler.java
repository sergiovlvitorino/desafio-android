package br.com.challenges.vitorino.gitranking.connect;

/**
 * Created by sergio on 18/02/16.
 */
public interface IResultHandler<T> {

    T getResult();

    void execute(Object o);

}
