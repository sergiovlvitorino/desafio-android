package br.com.challenges.vitorino.gitranking.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import br.com.challenges.vitorino.gitranking.bean.PullRequest;
import br.com.challenges.vitorino.gitranking.builder.PullRequestTableRowBuilder;
import br.com.challenges.vitorino.gitranking.parser.PullRequestParser;

/**
 * Created by sergio on 20/02/16.
 */
public class PullRequestsAdapter extends ArrayAdapter<PullRequest> {

    private PullRequestTableRowBuilder pullRequestTableRowBuilder;

    public PullRequestsAdapter(Context context, int resource) {
        super(context, resource);
        this.pullRequestTableRowBuilder = new PullRequestTableRowBuilder(LayoutInflater.from(context));
    }

    public PullRequestsAdapter(Context context){
        this(context , 0);
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PullRequest pullRequest = getItem(position);
        if (convertView == null){
            convertView = pullRequestTableRowBuilder.build(pullRequest);
        } else {
            convertView = pullRequestTableRowBuilder.build(convertView, pullRequest);
        }
        return convertView;
    }
}
