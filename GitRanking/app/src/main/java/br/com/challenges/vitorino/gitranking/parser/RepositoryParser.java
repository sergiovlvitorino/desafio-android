package br.com.challenges.vitorino.gitranking.parser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

import br.com.challenges.vitorino.gitranking.bean.Repository;
import br.com.challenges.vitorino.gitranking.util.Constants;

/**
 * Created by sergio on 19/02/16.
 */
public class RepositoryParser {

    public Repository convertJSONObjectToRepository(JSONObject jsonObject) throws JSONException {
        String title = jsonObject.getString(Constants.NAME);
        String description = jsonObject.getString(Constants.DESCRIPTION);
        int totalForks = jsonObject.getInt(Constants.FORKS);
        int totalWatches = jsonObject.getInt(Constants.WATCHERS);
        String fullname = jsonObject.getString(Constants.FULLNAME);
        JSONObject ownerJsonObject = jsonObject.getJSONObject(Constants.OWNER);
        String login = ownerJsonObject.getString(Constants.LOGIN);
        String avatarUrl = ownerJsonObject.getString(Constants.AVATAR_URL);
        return new Repository(title, description, totalForks, totalWatches, avatarUrl, login, fullname);
    }

    public List<Repository> convertJSONArrayToRepositoryList(JSONArray itemsJsonArray) throws JSONException {
        List<Repository> repositories = new ArrayList<>();
        for (int i = 0; i < itemsJsonArray.length(); i++){
            repositories.add(convertJSONObjectToRepository(itemsJsonArray.getJSONObject(i)));
        }
        return repositories;
    }
}
