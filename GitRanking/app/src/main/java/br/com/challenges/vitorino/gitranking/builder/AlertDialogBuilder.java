package br.com.challenges.vitorino.gitranking.builder;

import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.support.v7.app.AlertDialog;

import br.com.challenges.vitorino.gitranking.R;

/**
 * Created by sergio on 20/02/16.
 */
public class AlertDialogBuilder {

    public AlertDialog build(Context context){
        AlertDialog.Builder builder = new AlertDialog.Builder(context);
        builder.setTitle(context.getResources().getString(R.string.alert_title));
        builder.setMessage(context.getResources().getString(R.string.alert_message));
        return builder.create();
    }



}
