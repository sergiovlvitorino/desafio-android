package br.com.challenges.vitorino.gitranking.builder;

import android.app.Activity;
import android.app.ProgressDialog;

import br.com.challenges.vitorino.gitranking.R;

/**
 * Created by sergio on 20/02/16.
 */
public class ProgressDialogBuilder {

    public ProgressDialog build(Activity activity){
        ProgressDialog progressDialog = new ProgressDialog(activity, R.style.CustomDialog);
        progressDialog.setMessage(activity.getResources().getString(R.string.loading));
        progressDialog.setCancelable(false);
        progressDialog.setIndeterminate(true);
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        progressDialog.setCanceledOnTouchOutside(false);
        return progressDialog;
    }

}
