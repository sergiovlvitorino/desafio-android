package br.com.challenges.vitorino.gitranking.bean;

/**
 * Created by sergio on 19/02/16.
 */
public class Repository {

    private String title;
    private String description;
    private int totalForks;
    private int totalStars;
    private String urlAvatarImage;
    private String nicknameOwner;
    private String fullnameOwner;

    public Repository(){

    }

    public Repository(String title, String description, int totalForks, int totalStars, String urlAvatarImage, String nicknameOwner, String fullnameOwner) {
        this.title = title;
        this.description = description;
        this.totalForks = totalForks;
        this.totalStars = totalStars;
        this.urlAvatarImage = urlAvatarImage;
        this.nicknameOwner = nicknameOwner;
        this.fullnameOwner = fullnameOwner;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getTotalForks() {
        return totalForks;
    }

    public void setTotalForks(int totalForks) {
        this.totalForks = totalForks;
    }

    public int getTotalStars() {
        return totalStars;
    }

    public void setTotalStars(int totalStars) {
        this.totalStars = totalStars;
    }

    public String getUrlAvatarImage() {
        return urlAvatarImage;
    }

    public void setUrlAvatarImage(String urlAvatarImage) {
        this.urlAvatarImage = urlAvatarImage;
    }

    public String getNicknameOwner() {
        return nicknameOwner;
    }

    public void setNicknameOwner(String nicknameOwner) {
        this.nicknameOwner = nicknameOwner;
    }

    public String getFullnameOwner() {
        return fullnameOwner;
    }

    public void setFullnameOwner(String fullnameOwner) {
        this.fullnameOwner = fullnameOwner;
    }
}
