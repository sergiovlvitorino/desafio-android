package br.com.challenges.vitorino.gitranking;

import android.app.ProgressDialog;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import br.com.challenges.vitorino.gitranking.adapter.PullRequestsAdapter;
import br.com.challenges.vitorino.gitranking.bean.PullRequest;
import br.com.challenges.vitorino.gitranking.controller.PullRequestCtrl;
import br.com.challenges.vitorino.gitranking.util.Constants;
import br.com.challenges.vitorino.gitranking.builder.ProgressDialogBuilder;
import br.com.challenges.vitorino.gitranking.connect.InternetChecker;

/**
 * Created by sergio on 20/02/16.
 */
public class PullRequestActivity extends AppCompatActivity{

    private ListView listView;
    private PullRequestsAdapter pullRequestsAdapter;
    private PullRequestCtrl pullRequestCtrl;
    private TextView openedTextView, closedTextView;
    private ProgressDialog progressDialog;
    private static List<PullRequest> pullRequests;
    private static boolean firstExecution = true;
    private InternetChecker internetChecker;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pull_request);
        initializer();
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        toolbar.setTitle(getIntent().getExtras().getString(Constants.TITLE));
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setDisplayShowHomeEnabled(true);

        toolbar.setNavigationIcon(getResources().getDrawable(R.drawable.ic_action_back));
        toolbar.setNavigationOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onBackPressed();
            }
        });

        initializer();

        if (firstExecution){
            firstExecution = false;
            progressDialog.show();
            pullRequests = new ArrayList<>();
            loadList(getIntent().getExtras().getString(Constants.OWNER),
                    getIntent().getExtras().getString(Constants.TITLE));
        }
    }

    private void loadList(final String owner, final String title) {
        new Thread(new Runnable() {
            @Override
            public void run() {
                if (internetChecker.hasActiveInternetConnection()){
                    try {
                        renderingTable(pullRequestCtrl.getPullRequests(owner, title));
                    } catch (Exception e) {
                        launchToast(getResources().getString(R.string.error));
                    }
                } else {
                    launchToast(getResources().getString(R.string.no_internet_connection));
                }
                progressDialog.dismiss();
            }
        }).start();
    }

    private void renderingTable(final List<PullRequest> pullRequestsInner) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pullRequests.addAll(pullRequestsInner);
                pullRequestsAdapter.addAll(pullRequestsInner);
                renderingTotals(pullRequestsInner);
            }
        });
    }

    private void reRenderingTable(final List<PullRequest> pullRequestsInner, final int currentPosition) {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pullRequestsAdapter.addAll(pullRequestsInner);
                listView.setVerticalScrollbarPosition(currentPosition);
                renderingTotals(pullRequests);
            }
        });
    }

    private void renderingTotals(List<PullRequest> pullRequestsInner) {
        int totalClosed = 0;
        for(PullRequest pullRequest : pullRequestsInner){
            if (pullRequest.isClosed())
                totalClosed++;
        }
        int totalOpened = pullRequestsInner.size() - totalClosed;

        StringBuilder openedStringBuilder = new StringBuilder(String.valueOf(totalOpened));
        openedStringBuilder.append(Constants.SPACE);
        openedStringBuilder.append(getResources().getString(R.string.opened));
        openedStringBuilder.append(Constants.SPACE);
        openedTextView.setText(openedStringBuilder);

        StringBuilder closedStringBuilder = new StringBuilder(Constants.SLASH);
        closedStringBuilder.append(Constants.SPACE);
        closedStringBuilder.append(String.valueOf(totalClosed));
        closedStringBuilder.append(Constants.SPACE);
        closedStringBuilder.append(getResources().getString(R.string.closed));
        closedTextView.setText(closedStringBuilder);
    }


    private void initializer() {
        internetChecker = new InternetChecker(this);
        progressDialog = new ProgressDialogBuilder().build(this);
        openedTextView = (TextView) findViewById(R.id.opened);
        closedTextView = (TextView) findViewById(R.id.closed);
        listView = (ListView) findViewById(R.id.pull_request_list);
        pullRequestsAdapter = new PullRequestsAdapter(this);
        listView.setAdapter(pullRequestsAdapter);
        pullRequestCtrl = new PullRequestCtrl();
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent = new Intent(Intent.ACTION_VIEW);
                intent.setData(Uri.parse(pullRequestsAdapter.getItem(position).getUrlHtml()));
                startActivity(intent);
            }
        });
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
        this.overridePendingTransition(0, 0);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt(Constants.CURRENT_POSITION, listView.getFirstVisiblePosition());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        reRenderingTable(pullRequests, savedInstanceState.getInt(Constants.CURRENT_POSITION));
        super.onRestoreInstanceState(savedInstanceState);
    }

    @Override
    public void finish() {
        firstExecution = true;
        super.finish();
    }

    public void launchToast(final String message){
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                Toast.makeText(PullRequestActivity.this, message, Toast.LENGTH_LONG).show();
            }
        });
    }
}
