package br.com.challenges.vitorino.gitranking.connect;

/**
 * Created by sergio on 20/02/16.
 */
public class StringResultHandler implements IResultHandler<String> {

    private String result;

    @Override
    public String getResult() {
        return result;
    }

    @Override
    public void execute(Object o) {
        result = (String) o;
    }
}
