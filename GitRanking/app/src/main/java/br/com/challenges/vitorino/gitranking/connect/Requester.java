package br.com.challenges.vitorino.gitranking.connect;

import android.util.Log;

import com.loopj.android.http.BaseJsonHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import br.com.challenges.vitorino.gitranking.util.Constants;
import cz.msebera.android.httpclient.Header;

/**
 * Created by sergio on 18/02/16.
 */
public class Requester {


    public void doGet(String urlString, final IResultHandler resultHandler){
        SyncHttpClient syncHttpClient = new SyncHttpClient();
        RequestParams requestParams = new RequestParams();
        syncHttpClient.addHeader(Constants.USER_AGENT, Constants.USER_AGENT_ANSWER);
        syncHttpClient.get(urlString, requestParams, new BaseJsonHttpResponseHandler("UTF-8") {

            @Override
            public void onSuccess(int statusCode, Header[] headers, String rawJsonResponse, Object response) {
                StringBuilder text = new StringBuilder();
                if (statusCode == 200) {
                    if (rawJsonResponse.startsWith("\"")) {
                        text.append(rawJsonResponse.substring(1, rawJsonResponse.length() - 1));
                    } else {
                        text.append(rawJsonResponse);
                    }
                }
                resultHandler.execute(text.toString());
            }

            @Override
            public void onFailure(int statusCode, Header[] headers, Throwable throwable, String rawJsonData, Object errorResponse) {
                Log.e("Error", "Statuscode " + statusCode);
            }

            @Override
            protected Object parseResponse(String rawJsonData, boolean isFailure) throws Throwable {
                return null;
            }
        });
    }


}
