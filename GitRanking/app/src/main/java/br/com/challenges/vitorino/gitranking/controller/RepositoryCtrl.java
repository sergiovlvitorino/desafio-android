package br.com.challenges.vitorino.gitranking.controller;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.List;

import br.com.challenges.vitorino.gitranking.bean.Repository;
import br.com.challenges.vitorino.gitranking.connect.IResultHandler;
import br.com.challenges.vitorino.gitranking.connect.Requester;
import br.com.challenges.vitorino.gitranking.connect.StringResultHandler;
import br.com.challenges.vitorino.gitranking.util.Constants;
import br.com.challenges.vitorino.gitranking.parser.RepositoryParser;

/**
 * Created by sergio on 19/02/16.
 */
public class RepositoryCtrl {

    private Requester requester;
    private RepositoryParser repositoryParser;

    public RepositoryCtrl(){
        this.requester = new Requester();
        this.repositoryParser = new RepositoryParser();
    }

    public List<Repository> getRepositories(int page) throws JSONException {
        StringBuilder urlStringBuilder = new StringBuilder(Constants.URL_REPOSITORIES);
        urlStringBuilder.append(page);
        IResultHandler<String> resultHandler = new StringResultHandler();
        requester.doGet(urlStringBuilder.toString(), resultHandler);
        String requestResult = resultHandler.getResult();
        JSONObject requestResultJsonObject = new JSONObject(requestResult);
        JSONArray itemsJsonArray = requestResultJsonObject.getJSONArray(Constants.ITEMS);
        return repositoryParser.convertJSONArrayToRepositoryList(itemsJsonArray);
    }

}
