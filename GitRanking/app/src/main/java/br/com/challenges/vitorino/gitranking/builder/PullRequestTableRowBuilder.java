package br.com.challenges.vitorino.gitranking.builder;

import android.view.LayoutInflater;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;

import br.com.challenges.vitorino.gitranking.R;
import br.com.challenges.vitorino.gitranking.bean.PullRequest;

/**
 * Created by sergio on 20/02/16.
 */
public class PullRequestTableRowBuilder {


    private LayoutInflater layoutInflater;
    private SimpleDateFormat simpleDateFormat;

    public PullRequestTableRowBuilder(LayoutInflater layoutInflater) {
        this.layoutInflater = layoutInflater;
        this.simpleDateFormat = new SimpleDateFormat("'Created At: 'dd/MM/yyyy");
    }

    public View build(PullRequest pullRequest){
        View pullRequestTableRow = layoutInflater.inflate(R.layout.pull_request_table_row, null);
        return build(pullRequestTableRow, pullRequest);
    }

    public View build(View pullRequestTableRow, PullRequest pullRequest) {

        TextView pullRequestTitle = (TextView) pullRequestTableRow.findViewById(R.id.pull_request_title);
        pullRequestTitle.setText(pullRequest.getTitle());

        TextView pullRequestBody = (TextView) pullRequestTableRow.findViewById(R.id.pull_request_body);
        pullRequestBody.setText(pullRequest.getBody());

        ImageView OwnerImageView = (ImageView) pullRequestTableRow.findViewById(R.id.image_owner);
        Picasso.with(layoutInflater.getContext()).load(pullRequest.getUrlAvatarImage())
                .placeholder(R.drawable.person)
                .error(R.drawable.person)
                .into(OwnerImageView);

        TextView pullRequestCreatedAt = (TextView) pullRequestTableRow.findViewById(R.id.pull_request_created_at);
        pullRequestCreatedAt.setText(simpleDateFormat.format(pullRequest.getCreatedAt().getTime()));

        TextView pullRequestNickname = (TextView) pullRequestTableRow.findViewById(R.id.pulll_request_nickname);
        pullRequestNickname.setText(pullRequest.getNicknameOwner());

        return pullRequestTableRow;
    }
}
