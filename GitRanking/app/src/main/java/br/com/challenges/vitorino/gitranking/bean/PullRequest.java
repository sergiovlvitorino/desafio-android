package br.com.challenges.vitorino.gitranking.bean;

import java.util.Calendar;

/**
 * Created by sergio on 20/02/16.
 */
public class PullRequest {

    private String title;
    private String body;
    private String urlHtml;
    private String nicknameOwner;
    private String urlAvatarImage;
    private Calendar createdAt;
    private Boolean closed;

    public PullRequest(String title, String body, String urlHtml, String nicknameOwner, String urlAvatarImage, Calendar createdAt, Boolean closed) {
        this.title = title;
        this.body = body;
        this.urlHtml = urlHtml;
        this.nicknameOwner = nicknameOwner;
        this.urlAvatarImage = urlAvatarImage;
        this.createdAt = createdAt;
        this.closed = closed;
    }

    public PullRequest(){

    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getUrlHtml() {
        return urlHtml;
    }

    public void setUrlHtml(String urlHtml) {
        this.urlHtml = urlHtml;
    }

    public String getNicknameOwner() {
        return nicknameOwner;
    }

    public void setNicknameOwner(String nicknameOwner) {
        this.nicknameOwner = nicknameOwner;
    }

    public String getUrlAvatarImage() {
        return urlAvatarImage;
    }

    public void setUrlAvatarImage(String urlAvatarImage) {
        this.urlAvatarImage = urlAvatarImage;
    }

    public Calendar getCreatedAt() {
        return createdAt;
    }

    public void setCreatedAt(Calendar createdAt) {
        this.createdAt = createdAt;
    }

    public Boolean isClosed() {
        return closed;
    }

    public void setClosed(Boolean closed) {
        this.closed = closed;
    }
}
