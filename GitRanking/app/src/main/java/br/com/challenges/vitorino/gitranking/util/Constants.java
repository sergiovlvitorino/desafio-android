package br.com.challenges.vitorino.gitranking.util;

/**
 * Created by sergio on 19/02/16.
 */
public class Constants {

    public static final String FORKS = "forks";
    public static final String WATCHERS = "watchers";
    public static final String AVATAR_URL = "avatar_url";
    public static final String OWNER = "owner";
    public static final String DESCRIPTION = "description";
    public static final String ITEMS = "items";
    public static final String LOGIN = "login";
    public static final String NAME = "name";
    public static final String FULLNAME = "full_name";
    public static final String URL_REPOSITORIES = "https://api.github.com/search/repositories?q=language:Java&sort=stars&page=";
    public static final String USER_AGENT = "User-Agent";
    public static final String USER_AGENT_ANSWER = "sergiovlvitorino";
    public static final String URL_PULL_REQUESTS = "https://api.github.com/repos/";
    public static final String SLASH = "/";
    public static final String PULLS = "pulls";
    public static final String TITLE = "title";
    public static final String CLOSED_AT = "closed_at";
    public static final String BODY = "body";
    public static final String HTML_URL = "html_url";
    public static final String USER = "user";
    public static final String CREATED_AT = "created_at";
    public static final String CURRENT_POSITION = "current_position";
    public static final char SPACE = ' ';
    public static final String GOOGLE = "http://www.google.com.br/";
    public static final String TEST = "Test";
    public static final String CONNECTION = "Connection";
    public static final String CLOSE = "close";
    public static final int IMAGE_SPACE = 250;
}
