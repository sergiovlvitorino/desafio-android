package br.com.challenges.vitorino.gitranking.builder;

import android.content.Context;
import android.graphics.Point;
import android.view.Display;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import br.com.challenges.vitorino.gitranking.R;
import br.com.challenges.vitorino.gitranking.bean.Repository;
import br.com.challenges.vitorino.gitranking.util.Constants;

/**
 * Created by sergio on 19/02/16.
 */
public class RepositoryTableRowBuilder {

    private LayoutInflater layoutInflater;
    private int width;

    public RepositoryTableRowBuilder(LayoutInflater layoutInflater){
        this.layoutInflater = layoutInflater;
        loadWidth();
    }

    private void loadWidth() {
        WindowManager wm = (WindowManager) layoutInflater.getContext().getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        width = size.x;
    }

    public View build(Repository repository){
        View repositoryTableRow = layoutInflater.inflate(R.layout.repository_table_row, null);
        return build(repositoryTableRow, repository);
    }

    public View build(View repositoryTableRow, Repository repository){

        TextView repositoryTitle = (TextView) repositoryTableRow.findViewById(R.id.repository_title);
        repositoryTitle.setText(repository.getTitle());

        TextView repositoryDescription = (TextView) repositoryTableRow.findViewById(R.id.repository_description);
        repositoryDescription.setText(repository.getDescription());
        repositoryDescription.setWidth(width - Constants.IMAGE_SPACE);

        TextView totalForks = (TextView) repositoryTableRow.findViewById(R.id.total_forks);
        totalForks.setText(String.valueOf(repository.getTotalForks()));

        TextView totalWatches = (TextView) repositoryTableRow.findViewById(R.id.total_watches);
        totalWatches.setText(String.valueOf(repository.getTotalStars()));

        ImageView imageOwner = (ImageView) repositoryTableRow.findViewById(R.id.image_owner);
        Picasso.with(layoutInflater.getContext()).load(repository.getUrlAvatarImage())
                .placeholder(R.drawable.person)
                .error(R.drawable.person)
                .into(imageOwner);

        TextView nickname = (TextView) repositoryTableRow.findViewById(R.id.nickname);
        nickname.setText(repository.getNicknameOwner());

        TextView fullname = (TextView) repositoryTableRow.findViewById(R.id.fullname);
        fullname.setText(repository.getFullnameOwner());

        return repositoryTableRow;
    }

}
