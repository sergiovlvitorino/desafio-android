package br.com.challenges.vitorino.gitranking.connect;

import android.test.InstrumentationTestCase;

/**
 * Created by sergio on 21/02/16.
 */
public class InternetCheckerTest extends InstrumentationTestCase {

    private InternetChecker internetChecker;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        internetChecker = new InternetChecker(getInstrumentation().getTargetContext());
    }

    public void testIfActiveInternetConnectionIsOk() throws InterruptedException {
        final IResultHandler<Boolean> resultHandler = new IResultHandler<Boolean>() {
            private Boolean result;
            @Override
            public Boolean getResult() {
                return result;
            }
            @Override
            public void execute(Object o) {
                result = (Boolean) o;
            }
        };
        new Thread(new Runnable() {
            @Override
            public void run() {
                resultHandler.execute(internetChecker.hasActiveInternetConnection());
            }
        }).start();
        int count = 0;
        int timeout = 10;
        while (count++ < timeout & resultHandler.getResult() == null)
            Thread.sleep(1000);

        assertTrue(resultHandler.getResult());
    }
}
