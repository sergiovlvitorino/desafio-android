package br.com.challenges.vitorino.gitranking;

import android.test.ActivityInstrumentationTestCase2;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;

import com.robotium.solo.Solo;

/**
 * Created by sergio on 20/02/16.
 */
public class MainActivityTest extends ActivityInstrumentationTestCase2<MainActivity>{

    private Solo solo;

    public MainActivityTest(){
        super(MainActivity.class);
    }

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        solo = new Solo(getInstrumentation(), getActivity());
    }

    @Override
    protected void tearDown() throws Exception {
        solo.finishOpenedActivities();
        super.tearDown();
        Thread.sleep(1000);
    }

    public void testNavigation() throws InterruptedException {
        Thread.sleep(5000);
        int count = 0;
        int timeout = 10;
        while (count++ < timeout){
            Thread.sleep(1000);
            assertTrue(solo.scrollDown());
        }

        ListView listView = (ListView) solo.getView(R.id.repository_list);
        listView.setVerticalScrollbarPosition(0);
        solo.clickOnView(listView.getChildAt(0));
        Thread.sleep(5000);
        assertEquals(PullRequestActivity.class, solo.getCurrentActivity().getClass());

        solo.goBack();
        solo.scrollDown();
        assertEquals(MainActivity.class, solo.getCurrentActivity().getClass());

        solo.clickOnView(listView.getChildAt(0));
        Thread.sleep(1000);
        solo.goBack();
        Thread.sleep(1000);
        solo.scrollDown();
        assertEquals(PullRequestActivity.class, solo.getCurrentActivity().getClass());
    }

}
