package br.com.challenges.vitorino.gitranking.controller;

import android.test.InstrumentationTestCase;

import org.json.JSONException;

import java.util.List;

import br.com.challenges.vitorino.gitranking.bean.Repository;
import br.com.challenges.vitorino.gitranking.connect.IResultHandler;

/**
 * Created by sergio on 19/02/16.
 */
public class RepositoryCtrlTest extends InstrumentationTestCase {

    private RepositoryCtrl repositoryCtrl;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        repositoryCtrl = new RepositoryCtrl();
    }

    @Override
    protected void tearDown() throws Exception {
        super.tearDown();
        repositoryCtrl = null;
    }

    public void testIfGetRepositoriesIsOk() throws InterruptedException {
        final IResultHandler<List> resultHandler = new IResultHandler<List>() {
            private List<Repository> repositories;
            @Override
            public List getResult() {
                return repositories;
            }

            @Override
            public void execute(Object o) {
                repositories = (List) o;
            }
        };
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    List<Repository> repositories = repositoryCtrl.getRepositories(1);
                    assertFalse(repositories.isEmpty());
                    resultHandler.execute(repositories);
                } catch (Exception e) {
                    fail(e.getMessage());
                }
            }
        });
        thread.start();
        int count = 0;
        int timeout = 180;
        while (count != timeout && resultHandler.getResult() == null){
            Thread.sleep(1000);
            count++;
        }
        assertNotSame(count, timeout);
    }
}
