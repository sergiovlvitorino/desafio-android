package br.com.challenges.vitorino.gitranking.connect;

import android.content.Context;
import android.os.SystemClock;
import android.test.InstrumentationTestCase;
import android.os.Handler;

/**
 * Created by sergio on 18/02/16.
 */
public class RequesterTest extends InstrumentationTestCase {

    private Requester requester;
    private Context targetContext;

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        this.targetContext = getInstrumentation().getTargetContext();
        this.requester = new Requester();
    }

    public void testDoGetIsOk() throws InterruptedException {
        final IResultHandler<String> resultHandler = new IResultHandler<String>() {
            private String result = null;
            @Override
            public String getResult() {
                return result;
            }

            @Override
            public void execute(Object o) {
                result = (String) o;
            }
        };
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                requester.doGet("http://www.google.com.br/", resultHandler);
            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
        int count = 0;
        int timeout = 60;
        while (++count < timeout && resultHandler.getResult() == null) {
            Thread.sleep(1000);
        }
        assertFalse(resultHandler.getResult().isEmpty());
    }
}
