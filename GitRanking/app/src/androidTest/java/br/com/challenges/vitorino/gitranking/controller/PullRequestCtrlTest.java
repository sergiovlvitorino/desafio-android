package br.com.challenges.vitorino.gitranking.controller;

import android.test.InstrumentationTestCase;

import java.util.List;

import br.com.challenges.vitorino.gitranking.bean.PullRequest;
import br.com.challenges.vitorino.gitranking.connect.IResultHandler;

/**
 * Created by sergio on 20/02/16.
 */
public class PullRequestCtrlTest extends InstrumentationTestCase {

    private PullRequestCtrl pullRequestCtrl;
    public static final String FACEBOOK_LOGIN = "facebook";
    public static final String REPOSITORY_TITLE = "react-native";

    @Override
    protected void setUp() throws Exception {
        super.setUp();
        pullRequestCtrl = new PullRequestCtrl();
    }

    public void testIfGetPullRequestsIsOk() throws InterruptedException {
        final IResultHandler<List> resultHandler = new IResultHandler<List>() {
            private List<PullRequest> pullRequests;
            @Override
            public List getResult() {
                return pullRequests;
            }

            @Override
            public void execute(Object o) {
                pullRequests = (List) o;
            }
        };
        Thread thread = new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    List<PullRequest> pullRequests = pullRequestCtrl.getPullRequests(FACEBOOK_LOGIN, REPOSITORY_TITLE);
                    assertFalse(pullRequests.isEmpty());
                    resultHandler.execute(pullRequests);
                } catch (Exception e) {
                    fail(e.getMessage());
                }
            }
        });
        thread.start();
        int count = 0;
        int timeout = 180;
        while (count != timeout && resultHandler.getResult() == null){
            Thread.sleep(1000);
            count++;
        }
        assertNotSame(count, timeout);
    }
}
